{-# LANGUAGE BangPatterns #-}

module Algorithms.Random.Shuffle.Pure
  ( shuffle
  , sampleOne
  , sampleSplitOne
  , GetRandR
  ) where


import qualified Data.MonoTraversable  as MT
import qualified Data.Sequences        as S
import qualified System.Random.Shuffle as RS


-- | Monadic action generating an index number for shuffling.
--   The type parameter 'm' is usually some @Monad@.
--   The first argument is a inclusive range /(lo, hi)/.
--   So the retuned value should be /lo <= x <= hi/
--   (like 'System.Random.randomR' and 'System.Random.MWC.uniformR').
type GetRandR m = (Int, Int) -> m Int


-- | Reimplementation of 'RS.shuffleM' in terms of a raw @Monad m => m a@.
--   TODO: Generalize by mono-traversable
shuffle :: Monad m => GetRandR m -> [a] -> m [a]
shuffle _ [] = return []
shuffle randR xs =
  RS.shuffle xs <$> rseqM (length xs - 1)
 where
  rseqM 0 = return []
  rseqM i = (:) <$> randR (0, i) <*> rseqM (i - 1)
{-# INLINE shuffle #-}


-- | Implementation of <https://en.wikipedia.org/wiki/Reservoir_sampling Reservoir sampling>
--   for a single sample.
{-# INLINE sampleOne #-}
sampleOne :: (S.IsSequence seq, Monad m) => GetRandR m -> seq -> m (Maybe (MT.Element seq))
sampleOne randR xs =
  case S.uncons xs of
    Just (first, left) -> Just <$> sampleOneDef randR first left
    _                  -> return Nothing


{-# INLINE sampleOneDef #-}
sampleOneDef :: (S.IsSequence seq, Monad m) => GetRandR m -> MT.Element seq -> seq -> m (MT.Element seq)
sampleOneDef randR def = gSampleDef def f randR
 where
  {-# INLINE f #-}
  f shouldChoose candidate current =
    if shouldChoose then candidate else current


-- | Implementation of <https://en.wikipedia.org/wiki/Reservoir_sampling Reservoir sampling>
--   for a single sample. By contrast to 'sampleOne',
--   this function returns not chosen elements in addition.
{-# INLINE sampleSplitOne #-}
sampleSplitOne :: (S.IsSequence seq, Monad m) => GetRandR m -> seq -> m (Maybe (MT.Element seq, seq))
sampleSplitOne randR xs =
  case S.uncons xs of
    Just (first, left) -> Just <$> sampleSplitOneDef randR first left
    _                  -> return Nothing


{-# INLINE sampleSplitOneDef #-}
sampleSplitOneDef :: (S.IsSequence seq, Monad m) => GetRandR m -> MT.Element seq -> seq -> m (MT.Element seq, seq)
sampleSplitOneDef randR def = gSampleDef (def, mempty) f randR
 where
  {-# INLINE f #-}
  f shouldChoose candidate (!current, !notChosens) =
    if shouldChoose
      then (candidate, current `S.cons` notChosens)
      else (current, candidate `S.cons` notChosens)


{-# INLINE gSampleDef #-}
gSampleDef
  :: (MT.MonoFoldable mono, Monad m)
  => a
  -> (Bool -> MT.Element mono -> a -> a)
  -> GetRandR m
  -> mono
  -> m a
gSampleDef initial updateIf randR =
  fmap snd . MT.ofoldlM f (size, initial)
 where
  f (!i, !state) candidate = do
    j <- randR (0, i)
    let new = updateIf (j == 0) candidate state
    return (i + 1, new)
  size = 1
