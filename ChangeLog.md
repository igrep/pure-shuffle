
- Refactor and add `INLINE`s.

# 0.1.1.1

- Add `sampleSplitOne` function.
- Improve document of `GetRandR`.

# 0.1.0.1

- Fix: forgot to export `GetRandR` type synonym.

# 0.1.0.0

- Initial release.
