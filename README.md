# pure-shuffle

Provides `shuffle` function (and its related ones) depending only on `Monad` (without any specific random number generators).
